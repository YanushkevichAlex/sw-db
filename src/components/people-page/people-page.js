import React, {Component} from 'react';
import ItemList from '../item-list';
import ItemDetails from '../item-details';
import './people-page.css';
import SwapiService from "../../services/swapi-service";
import Row from "../row/row";
import {Record} from "../item-details/item-details";

export default class PeoplePage extends Component {

    swapiService = new SwapiService();

    state = {
        selectedPerson: null
    };

    onPersonSelected = (id) => {
        this.setState({
            selectedPerson: id
        })
    };


    render() {
        const itemList = (
            <ItemList onItemSelected={this.onPersonSelected}
                      getData={this.swapiService.getAllPeople} />
        );

        const {getPerson, getPersonImage} = this.swapiService;

        const personDetails = (
            <ItemDetails itemId={this.state.selectedPerson}
                         getData={getPerson}
                         getImageUrl={getPersonImage}>
                <Record field="gender" label="Gender"/>
                <Record field="eyeColor" label="Eye Color"/>
            </ItemDetails>);

        // const itemDetails = (
        //     <ItemDetails itemId={this.state.selectedPerson} />
        // );

        return (
            <Row left={itemList} right={personDetails}/>
        );
    }
}