import React, {Component} from 'react';
import Header from '../header';
import RandomPlanet from "../random-planet";

import './app.css';
import PeoplePage from "../people-page/people-page";
import ItemList from "../item-list/item-list";
import ItemDetails, {Record} from "../item-details/item-details";
import SwapiService from "../../services/swapi-service";
import Row from "../row/row";
import { BrowserRouter as Router, Route } from 'react-router-dom';

export default class App extends Component {

    swapiService = new SwapiService();

    render() {

        const {getPerson, getStarship, getPersonImage, getStarshipImage} = this.swapiService;

        const personDetails = (
            <ItemDetails itemId={11}
                         getData={getPerson}
                         getImageUrl={getPersonImage}>
                <Record field="gender" label="Gender"/>
                <Record field="eyeColor" label="Eye Color"/>
            </ItemDetails>);

        const starshipsDetails = (
            <ItemDetails itemId={5}
                         getData={getStarship}
                         getImageUrl={getStarshipImage}>
                <Record field="model" label="Model"/>
                <Record field="length" label="length"/>
                <Record field="costInCredits" label="Cost"/>
            </ItemDetails>);

        return (
            <div>
                <Router>
                    <Header/>
                    <RandomPlanet/>
                    <Route path="/" render={()=> <h2>Welcome to StarDB</h2>} exact/>
                    <Route path="/people" component={PeoplePage}/>
                </Router>
                {/*<Row left={personDetails} right={starshipsDetails}/>*/}
            </div>
        );
    }
};