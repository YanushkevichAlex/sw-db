import React, {Component} from 'react';
import Spinner from '../spinner/spinner';

import './item-list.css';

export default class ItemList extends Component {

    state = {
      itemList: null
    };

    componentDidMount() {

        const { getData } = this.props;

        getData()
            .then((itemList) => {
                this.setState({
                    itemList
                });
            });
    }

    render () {

        const {itemList} = this.state;

        if (!itemList) {
            return <Spinner/>
        }

        const elements = itemList.map(({id, name}) => {
            return (
                <li className="list-group-item"
                    key={id}
                    onClick={() => this.props.onItemSelected(id)}>
                    {name}
                </li>
            );
        });

        return (
            <ul className="item-list list-group">
                {elements}
            </ul>
        )
    }
};