import React from 'react';

import icon from './wtf.jpeg';

const ErrorIndicator = () => {
  return (
      <div>
          <img src={icon} alt="error-icon"/>
          <span>
              something has gone terribly wrong
          </span>
          <span>
              (but we already sent droids to fix it)
          </span>
      </div>
  );
};

export default ErrorIndicator;